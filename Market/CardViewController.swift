//
//  CardViewController.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/14/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {

    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var checkout: UIButton!
    var cartPresent = CardPresenter.getSharedInstance()
    let CELL_ID = "cell"
    var hub:YBHud?
    let loginPresenter:LoginPresenter = LoginPresenter()
    let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cartPresent.attacheView(cardView: self)

        self.table.register(UINib.init(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: CELL_ID)
        self.cartPresent.calclateTotalPrice()
        
        
        notify.onCheckoutData.subscribe(on: self) { (isUpdated) in
            self.hub?.dismiss(animated: true)
            if isUpdated {
                self.cartPresent.cartProducts = [String:ProductOnCart] () 
                self.table.reloadData()
            } else {
                let alert = UIAlertController(title: "خطاء", message: " حدث خطاء ما ،يرجي المحاولة لاحقا", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.cartPresent.detchView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.cartPresent.calclateTotalPrice()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onCheckOut(_ sender: AnyObject) {
        
        if cartPresent.cartProducts.count < 1 {
            return
        }
        
        hub = YBHud(hudType: .triplePulse)
        hub?.dimAmount = 0.7
        hub?.show(in: self.view, animated: true)
        
        cartPresent.checkOutProducts()
        
    }
    

}


extension CardViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cartPresent.cartProducts.keys.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! CartTableViewCell
        
        let productonCart = Array(cartPresent.cartProducts.values)[indexPath.row]
        let product = productonCart.product
        
        cell.title?.text = product?.name
        cell.productID = product?.id
        cell.productImage.sd_setImage(with: URL(string: (product?.image)!), placeholderImage: UIImage(named: "placeholder.png"))
        
        if !(product?.saleRate.isEmpty)! {
            cell.discount.isHidden = false
            cell.discount.text = (product?.saleRate)! + " %"
        } else {
            cell.discount.isHidden = true
        }
        cell.cartPresenter = self.cartPresent
        cell.price.text = ((product?.priceInSale)! + " SAR")
        cell.count.text = String.init(productonCart.count)
        cell.selectionStyle = .none;
        let price = product?.price
        cell.priceBeforeDiscount.text = "كان: \(price!) SAR"
        return cell
    }
    
    @objc(tableView:heightForRowAtIndexPath:) public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105;
    }
}

extension CardViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        // TODO:- push list of products
        
    }
}

extension CardViewController : CardPresenterDelegate {
    
    func updateTotalPrice(total:Double) {
        totalPrice.text = "الاجمالي : \(total) ريال"
    }
    
}
