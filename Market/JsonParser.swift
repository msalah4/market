//
//  JsonParser.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/20/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class JsonParser {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func getCategoryFromJson(_ dataDic:[[String: Any]]) -> Category {
        
        let category = Category()
        
        for cat in dataDic {
            
            category.catId = cat["CatID"] as! String
            category.name = cat["CatName"] as! String
            category.image = cat["CatImg"] as! String
        }
        
        return category
    }

}
