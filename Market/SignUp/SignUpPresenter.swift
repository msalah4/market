//
//  LoginPresenter.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import  RealmSwift
class SignUpPresenter: NSObject {
    
    
    func signup(_ listOfinputs:[String]){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        let user = User()
        
        let userData = ["tem:FullName":listOfinputs[0], "tem:Area":listOfinputs[1], "tem:Address":listOfinputs[2], "tem:Email":listOfinputs[3],
                        "tem:Password":listOfinputs[4], "tem:Mobile1":listOfinputs[5], "tem:Mobile2":listOfinputs[6], "tem:Mobile3":listOfinputs[7],
                        "tem:Mobile4":listOfinputs[8], "tem:IDCode":listOfinputs[9], "tem:StreetName":listOfinputs[10] ]
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:NewUser",
                                    children: userData,
                                    elementName: ["NewUserResponse", "NewUserResult"],
                                    getStringResult:true,
                                    completion: {
                                        result in
                                        
                                        if result == nil {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        let value = result![0]["result"] as! String
                                        
                                        if value.characters.count > 10 {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        // Parse data
                                        let realm = try! Realm()
                                        
                                        user.fullName = listOfinputs[0]
                                        user.area = listOfinputs[1]
                                        user.address = listOfinputs[2]
                                        user.email = listOfinputs[3]
                                        user.password = listOfinputs[4]
                                        user.mobile1 = listOfinputs[5]
                                        user.mobile2 = listOfinputs[6]
                                        user.mobile3 = listOfinputs[7]
                                        user.mobile4 = listOfinputs[8]
                                        user.idCode = result![0]["result"] as! String
                                        user.streetName = listOfinputs[10]
                                        
                                        try! realm.write {
                                            realm.add(user, update: true)
                                        }
                                           
                                        // Notify
                                        notify.onUpdateUserData.fire(true)
        })
        

    }

}
