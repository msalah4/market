//
//  LoginViewController.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit


class SignUpViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var hub:YBHud?
    let signUpPresenter:SignUpPresenter = SignUpPresenter()
    let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
    let types: [CellsTypes]  = [.normal, .normal,
                                                .normal, .Email,
                                                .Password,
                                                .Phone, .Phone,
                                                .Phone, .Phone,
                                                .normal]
    
    let titles = ["الاسم كامل", "المنطقة", "العنوان", "الايميل",
                  "الباسورد", "جوال 1", "جوال 2", "جوال 3",
                  "جوال 4", "اسم الشارع"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        notify.onUpdateUserData.subscribe(on: self) { (isUpdated) in
            self.hub?.dismiss(animated: true)
            if isUpdated {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "home")
                self.navigationController?.viewControllers = [viewController!]
            } else {
                let alert = UIAlertController(title: "خطاء", message: " حدث خطاء ما ،يرجي المحاولة لاحقا", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        

        CellaManager.registerTableView(tableView)
        tableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func onSignUp(_ sender: UIButton) {
        
        
        var arrayOfInputs = [String]()
        for index in 0...9 {
            
            
            let indexPath = IndexPath(row: index, section: 0)
            let cell  = self.tableView.cellForRow(at: indexPath) as! FieldCell
            
            if index == 9 {
                arrayOfInputs.append("")
                
              if (cell.txtfield.text?.isEmpty)! {
                let alert = UIAlertController(title: "خطاء", message: "برجاء ملئ الخانات المطلوبة", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
                }
            }
            
            arrayOfInputs.append(cell.txtfield.text!)
        }
        
        
        hub = YBHud(hudType: .triplePulse)
        hub?.dimAmount = 0.7
        hub?.show(in: self.view, animated: true)
        
        signUpPresenter.signup(arrayOfInputs)
        
    }
    
    
    

}

extension SignUpViewController: UITableViewDelegate {
    
    
}

extension SignUpViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType: CellsTypes = types[indexPath.row]
        let cell = CellaManager.instanceFromNib(cellType)//tableView.dequeueReusableCell(withIdentifier: cellType.rawValue, for: indexPath)
        cell.cellType = cellType
        (cell as! (FieldCell)).title.text = titles[indexPath.row]
        cell.cellDidLoad(cellType)
        cell.selectionStyle = .none
        
        if indexPath.row == 9 {
            (cell as! (FieldCell)).fieldType = .Mandatory
        }
        
        (cell as! (FieldCell)).txtfield.placeholder = (cell as! (FieldCell)).fieldType.rawValue
        
        return cell;
        
        
    }
    
}
