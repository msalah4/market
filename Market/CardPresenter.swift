//
//  CardPresenter.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/14/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift
var shardInstance:CardPresenter = CardPresenter()


protocol CardPresenterDelegate {
    func updateTotalPrice(total:Double)
}

class CardPresenter: NSObject {
    
    var cartProducts = [String:ProductOnCart] ()
    var cartView:CardPresenterDelegate?
    var barButton:BadgedBarButtonItem?
    
    
    
    let realm = try! Realm()
    
    func checkOutProducts() {
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        let user = realm.objects(User.self)[0]
        
        let numberOfRequests = cartProducts.count
        var successRequests = 0
        var failsRequests = 0
        
        for item in cartProducts {
        
            
            let userData = ["tem:UserID":user.loginCode, "tem:ProdID":item.key, "tem:Quantity":String(item.value.count)]
            
            ServiceManager.retrieveData(actionName: "tem:FillCart",
                                        children: userData,
                                        elementName: ["FillCartResponse", "FillCartResult"],
                                        getStringResult:true,
                                        completion: {
                                            result in
                                            
                                            if result == nil {
                                                notify.onUpdateUserData.fire(false)
                                            }
                                            
                                            let value = result![0]["result"] as! String
                                            
                                            if value.characters.count < 1 {
                                                failsRequests += 1
                                            } else {
                                                successRequests += 1
                                            }
                                        
                                            if (failsRequests + successRequests) == numberOfRequests {
                                                
                                                if failsRequests > successRequests {
                                                    
                                                    // Notify
                                                    notify.onCheckoutData.fire(false)
                                                } else {
                                                    
                                                    
                                                    ServiceManager.retrieveData(actionName: "tem:AddOrder",
                                                                                children: ["UserID":user.loginCode],
                                                                                elementName: ["AddOrderResponse", "AddOrderResult"],
                                                                                getStringResult:true,
                                                                                completion: {
                                                                                    result in
                                                                                    
                                                                                    if result == nil {
                                                                                        notify.onCheckoutData.fire(false)
                                                                                    }
                                                                                    
                                                                                    let value = result![0]["result"] as! String
                                                                                    
                                                                                    if value.characters.count > 10 {
                                                                                        notify.onCheckoutData.fire(false)
                                                                                    }
                                                                                    
                                                                                    // Notify
                                                                                    notify.onCheckoutData.fire(true)
                                                    })

                                                }
                                            }
                                            
            })

        }
        
        
    }
    
    func attacheView(cardView: CardPresenterDelegate){
        self.cartView = cardView
    }
    
    func detchView(){
        self.cartView = nil
    }
    
    func updateCartBadge(){
        barButton?.badgeValue = self.cartProducts.count
    }
    
    class func getSharedInstance() -> CardPresenter {
        return shardInstance
    }
    
    func addProduct(product:Product){
        shardInstance.cartProducts.updateValue(ProductOnCart(product), forKey: product.id)
        self.updateCartBadge()
    }
    
    func addProduct(productID:String){
        
        let product = shardInstance.realm.objects(Product.self).filter("id == %@", productID)[0]
        shardInstance.cartProducts.updateValue(ProductOnCart(product), forKey: product.id)
        self.updateCartBadge()
    }
    
    func removeProduct(product:Product){
        shardInstance.cartProducts.removeValue(forKey: product.id)
        self.updateCartBadge()
    }

    func removeProduct(productID:String){
        shardInstance.cartProducts.removeValue(forKey: productID)
        self.updateCartBadge()
    }
    
    func getProduct(productID:String) ->ProductOnCart {
        return shardInstance.cartProducts[productID]!
    }
    
    func plusCounOfProduct(productID:String) -> Int {
        let product = shardInstance.cartProducts[productID]!
        product.count += 1
        
        calclateTotalPrice()
        
        return product.count
        
        
    }
    
    func isProductInCart(productID:String) -> Bool {
        let product = shardInstance.cartProducts[productID]!
        
        return (product != nil)
    }
    
    func minusCounOfProduct(productID:String) -> Int {
        let product = shardInstance.cartProducts[productID]!
        
        if product.count > 0 {
            product.count -= 1
        }
        
        calclateTotalPrice()
        
        return product.count
    }
    
    func calclateTotalPrice() {
        
        var total = 0.0
        
        if cartProducts.count > 0 {
            
            for product in (Array(cartProducts.values) as! [ProductOnCart]) {
                
                total += product.getProductTotalPrice()
            }
        }
        
        if self.cartView != nil {
            self.cartView?.updateTotalPrice(total: total)
        }
    }
}
