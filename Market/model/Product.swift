//
//  Product.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class Product: Object {
    
    dynamic var id = ""
    dynamic var catId = ""
    dynamic var name = ""
    dynamic var image = ""
    dynamic var price = ""
    dynamic var priceInSale = ""
    dynamic var desc = ""
    dynamic var saleRate = ""
    dynamic var weightUnit = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
