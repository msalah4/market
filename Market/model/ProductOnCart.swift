//
//  ProductOnCart.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/28/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class ProductOnCart: NSObject {
    
    
    var product:Product?
    var count = 1
    var actualPrice = 0.0
    
    init(_ product:Product) {
        super.init()
        self.product = product
       
        self.actualPrice = Double((self.product?.price)!)!
        if let priceInDiscount = Double((self.product?.priceInSale)!) {
            
            if priceInDiscount > 0.0 {
                self.actualPrice = priceInDiscount
            }
        }
    }
    
    func getProductTotalPrice () -> Double {
        return Double(count) * actualPrice
    }
    
}
