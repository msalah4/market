//
//  Order.swift
//  Market
//
//  Created by Mohamed Salah on 10/9/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class Order: NSObject {
    
    var orderID = -1
    var total = ""
    var date = ""
    var accepted = ""
    var marketCode = ""

}
