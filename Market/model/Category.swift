//
//  Category.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class Category: Object {
    
    dynamic var name = ""
    dynamic var image  = ""
    dynamic var catId = ""
    
    override static func primaryKey() -> String? {
        return "catId"
    }

}
