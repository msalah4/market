//
//  User.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {
    
    dynamic var fullName = ""
    dynamic var area = ""
    dynamic var address = ""
    dynamic var email = ""
    dynamic var password = ""
    dynamic var mobile1 = ""
    dynamic var mobile2 = ""
    dynamic var mobile3 = ""
    dynamic var mobile4 = ""
    dynamic var idCode = ""
    dynamic var loginCode = ""
    dynamic var streetName = ""
    
    override static func primaryKey() -> String? {
        return "idCode"
    }

}
