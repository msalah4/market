//
//  AdminProduct.swift
//  Market
//
//  Created by Mohamed Salah on 10/9/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AdminProduct: NSObject {
    
    var itemID = 0
    var productName = ""
    var mainPrice = ""
    var mainPriceInSale = ""
    var Quantity = ""
    var unit = ""
    var actualPrice = ""
    var prodImage = ""
    var catName = ""
    
}
