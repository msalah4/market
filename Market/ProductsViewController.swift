//
//  ProductsViewController.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/14/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class ProductsViewController: ParentViewController {
    
    @IBOutlet weak var table: UITableView!
    var dataUpdateNotifocation:DataUpdateNotifocation?
    let realm = try! Realm()
    var selectedCategory: Category?
    var listOfProducts:Results<Product>?
    let CELL_ID = "cell"
    var cartPresent = CardPresenter.getSharedInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.register(UINib.init(nibName: "ProductsTableViewCell", bundle: nil), forCellReuseIdentifier: CELL_ID)
        
        dataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        dataUpdateNotifocation?.onProductData.subscribe(on: self) { (isUpdated) in
            if isUpdated {
                let catId:String = (self.selectedCategory?.catId)!
                self.listOfProducts = self.realm.objects(Product.self).filter("catId == %@", catId)
                self.table.reloadData()
            }
        }
        
        
        let catId:String = (self.selectedCategory?.catId)!
        listOfProducts = self.realm.objects(Product.self).filter("catId == %@", catId)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductsViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (listOfProducts?.count)!
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! ProductsTableViewCell
        
        let product = listOfProducts?[indexPath.row]
        
        cell.title?.text = product?.name
        cell.desc.text = product?.desc
        cell.productID = product?.id
        cell.productImage.sd_setImage(with: URL(string: (product?.image)!), placeholderImage: UIImage(named: "placeholder.png"))
        
        if !(product?.saleRate.isEmpty)! {
            cell.discount.isHidden = false
            cell.discount.text = (product?.saleRate)! + " %"
        } else {
            cell.discount.isHidden = true
        }

        cell.price.text = product?.price
        cell.selectionStyle = .none;
        return cell
    }
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105;
    }
}

extension ProductsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        // TODO:- push list of products
        
    }
}
