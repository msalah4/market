//
//  LoginViewController.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class HomeViewController: ParentViewController {
    
    let CELL_ID = "cell"
    
    var barButtonLogout:UIBarButtonItem?
    @IBOutlet weak var table: UITableView!
    var dataUpdateNotifocation:DataUpdateNotifocation?
    let realm = try! Realm()
    var categories:Results<Category>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.register(UINib.init(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: CELL_ID)
        
        dataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        dataUpdateNotifocation?.onCategoryData.subscribe(on: self) { (isUpdated) in
            if isUpdated {
                self.categories = self.realm.objects(Category.self)
                self.table.reloadData()
            }
        }
        self.categories = self.realm.objects(Category.self)
        
        barButtonLogout = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .done, target: self, action: #selector(logout(_:)))
        
        self.navigationItem.leftBarButtonItem =  barButtonLogout

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension HomeViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if categories == nil {
            return 0
        }
        return categories!.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! CategoriesCell
        
        let category = categories?[indexPath.row]
        
        cell.catTitle?.text = category?.name
        
        cell.catImage.sd_setImage(with: URL(string: (category?.image)!), placeholderImage: UIImage(named: "placeholder.png"))

        cell.selectionStyle = .none;
        
        return cell
    }
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    
    func logout(_ sender:Any){
        
        try! realm.write {
            realm.delete(realm.objects(User.self))
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "sign")
        self.navigationController?.setViewControllers([vc!], animated: true)
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
             
        let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "products") as! ProductsViewController
        productViewController.selectedCategory = categories?[indexPath.row]
        
        self.show(productViewController, sender: nil)
    }
}
