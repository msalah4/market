//
//  UpdateService.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/18/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class UpdateService: NSObject {
    
    class func updateDatabase(_ notify:DataUpdateNotifocation) {
        
        var categories = [Category]()
        var catsMap = [[String: Category]]()
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:Getcategories",
                                    children: nil,
                                    elementName: ["GetcategoriesResponse", "GetcategoriesResult"],
                                    completion: {
                                        result in
                                        
                                        if result == nil {
                                            
                                            notify.onCategoryData.fire(false)
                                        }
                                        
                                        // Parse data
                                        let realm = try! Realm()
                                        for catJson in result! {
                                            
                                            let category:Category = Category()
                                            category.catId = String((catJson["CatID"] as! Int))
                                            category.name = catJson["CatName"] as! String
                                            category.image = (catJson["CatImg"] as? String)!
                                            
                                            // 2- replace and update the database
                                            try! realm.write {
                                                realm.add(category, update: true)
                                            }
                                            categories.append(category)
                                            catsMap.append([category.catId: category])
                                        }
                                        
                                            // Notify
                                            notify.onCategoryData.fire(true)
                                        
                                        for category in categories {
                                            ServiceManager.retrieveData(actionName: "tem:GetProducts",
                                                                        children: ["tem:CategoryID":category.catId],
                                                                        elementName: ["GetProductsResponse", "GetProductsResult"],
                                                                        completion: {
                                                                            result in
                                                                            
                                                                            if result == nil {
                                                                                
                                                                                notify.onProductData.fire(false)
                                                                            }
                                                                            
                                                                            // Parse data
                                                                            let realm = try! Realm()
                                                                            for productJson in result! {
                                                                                
                                                                                let product:Product = Product()
                                                                                product.id = String((productJson["ProdID"] as! Int))
                                                                                product.image = productJson["ProdImg"] as! String
                                                                                product.price = productJson["ProdPrice"] as! String
                                                                                product.priceInSale = productJson["ProdPriceInSale"] as! String
                                                                                product.desc = productJson["ProdDescription"] as! String
                                                                                product.catId = category.catId
                                                                                product.saleRate = productJson["ProdSaleRate"] as! String
                                                                                product.name = productJson["ProdName"] as! String
                                                                                product.weightUnit = productJson["WeightUnit"] as! String
                                                                                
                                                                                // 2- replace and update the database
                                                                                try! realm.write {
                                                                                    realm.add(product, update: true)
                                                                                }
                                                                            }
                                                                            
                                                                            catsMap.remove(at: 0)
                                                                            
                                                                            if catsMap.count == 0 {
                                                                                // Notify
                                                                                notify.onProductData.fire(true)
                                                                            }
                                            })
                                        }
                                        

        })
        
        
        
        
    }
    
    
}
