//
//  AdminPresenter.swift
//  Market
//
//  Created by Mohamed Salah on 10/28/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AdminPresenter: NSObject {

    func addToken(_ token:String, _ devInfo:String){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        let user = User()
        
        let userData = ["tem:DeviceToken":token, "tem:DeviceInfo":devInfo]
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:AddAppleToken",
                                    children: userData,
                                    elementName: ["AddAppleTokenResponse", "AddAppleTokenResult"],
                                    getStringResult:true,
                                    completion: {
                                        result in
                                        
                                        if result == nil {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        let value = result![0]["result"] as! String
                                        
                                        if value.characters.count > 10 {
                                            notify.onRegisterTokenData.fire(false)
                                        }
                                        
                                        
                                        
                                        // Notify
                                        notify.onRegisterTokenData.fire(true)
                                        UserDefaults.standard.set(true, forKey: "hasToken")
                                        
        })
        
        
    }
    
    
    func deleteToken(_ token:String){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        let user = User()
        
        let userData = ["tem:DeviceToken":token]
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:DeleteAppleToken",
                                    children: userData,
                                    elementName: ["DeleteAppleTokenResponse", "DeleteAppleTokenResult"],
                                    getStringResult:true,
                                    completion: {
                                        result in
                                        
                                        if result == nil {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        let value = result![0]["result"] as! String
                                        
                                        if value.characters.count > 10 {
                                            notify.onRegisterTokenData.fire(false)
                                        }
                                        
                                        
                                        // Notify
                                        notify.onRegisterTokenData.fire(true)
                                        UserDefaults.standard.set(false, forKey: "hasToken")
                                        UserDefaults.standard.set(token, forKey: "token")
                                        
        })
        
        
    }

    
}
