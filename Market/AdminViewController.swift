//
//  AdminViewController.swift
//  Market
//
//  Created by Mohammed Salah on 10/7/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class AdminViewController: UIViewController {

    let CELL_ID = "cell"
    let adminPresnter = AdminPresenter()
    var barButton:UIBarButtonItem?
    var hub:YBHud?
    @IBOutlet weak var table: UITableView!
    var dataUpdateNotifocation:DataUpdateNotifocation?
    var orders:[Order] = [Order]()
    var timer:Timer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.register(UINib.init(nibName: "AdminTableViewCell", bundle: nil), forCellReuseIdentifier: CELL_ID)
        
        dataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        dataUpdateNotifocation?.onOrderData.subscribe(on: self) { (data) in
            
            self.hub?.dismiss(animated: true)
            self.orders = data
            self.table.reloadData()
                
        }
        
        
        
        hub = YBHud(hudType: .triplePulse)
        hub?.dimAmount = 0.7
        hub?.show(in: self.view, animated: true)
        
        let image = UIImage(imageLiteralResourceName: "logout")
        let buttonFrame: CGRect = CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height)
        
        barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .done, target: self, action: #selector(logout(_:)))
        
        self.navigationItem.leftBarButtonItem =  barButton
        
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(AdminViewController.getToken), userInfo: nil, repeats: true)
        
                
    }
    
    
    func getToken()
    {
        DispatchQueue.main.async {
            let token = (UIApplication.shared.delegate as! AppDelegate).tokenDev
            let isPreviouslyAdded = UserDefaults.standard.bool(forKey: "hasToken")
            
            if token.characters.count > 0 {
                if token != "error" && !isPreviouslyAdded {
                   // send token to backend
                    self.adminPresnter.addToken(token, "_")
                }
                  self.timer?.invalidate()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if orders.count == 0 {
            AdminService.fetchTodayOrders()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func logout(_ sender:Any){
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(User.self))
        }
        
        self.adminPresnter.deleteToken(UserDefaults.standard.string(forKey: "token")!)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "sign")
        self.navigationController?.setViewControllers([vc!], animated: true)
    }

}


extension AdminViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders == nil {
            return 0
        }
        return orders.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! AdminTableViewCell
        
        let order = orders[indexPath.row]
        
        cell.orderCode?.text = "كود الطلب: \(String(describing: order.orderID))"
        cell.marketCode?.text = "كود الماركت: \(String(describing: order.marketCode))"
        cell.date?.text = "\(String(describing: order.date))"
        cell.accepted?.text = "الحالة: \(String(describing: order.accepted))"
        
        if order.accepted == "True" {
            cell.accepted?.textColor = UIColor.green
            cell.accepted?.text = "الحالة: مقبول"
        } else {
            cell.accepted?.textColor = UIColor.red
            cell.accepted?.text = "الحالة: مرفوض"
        }
        cell.price.text = "\(order.total) SAR"
        cell.selectionStyle = .none;
        
        return cell
    }
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
}



extension AdminViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let adminDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "adminDetails") as! AdminDetailsViewController
        adminDetailsViewController.order = (orders[indexPath.row])
        
        self.show(adminDetailsViewController, sender: nil)
    }
}

