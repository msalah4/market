//
//  AdminDetailsViewController.swift
//  Market
//
//  Created by Mohamed Salah on 10/11/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AdminDetailsViewController: UIViewController {

    @IBOutlet weak var accept: UIButton!
    let CELL_ID = "cell"
    var order:Order = Order()
    var hub:YBHud?
    var orderStatus = true
    @IBOutlet weak var table: UITableView!
    var dataUpdateNotifocation:DataUpdateNotifocation?
    var adminProducts:[AdminProduct] = [AdminProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.register(UINib.init(nibName: "adminDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: CELL_ID)
        
        if order.orderID > 0 {
            AdminService.fetchProductsOfOrder(OrderID: String(order.orderID))
            
            hub = YBHud(hudType: .triplePulse)
            hub?.dimAmount = 0.7
            hub?.show(in: self.view, animated: true)
            
        }
        
        dataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        dataUpdateNotifocation?.onAdminProductData.subscribe(on: self) { (data) in
            self.adminProducts = data
            self.table.reloadData()
            self.hub?.dismiss(animated: true)
            
        }
        
        if adminProducts.count == 0  || (orderStatus) {
            accept.isEnabled = false
            accept.backgroundColor = UIColor.gray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func onAccept(_ sender: UIButton) {
        
        
        AdminService.acceptOrder(OrderID: String(order.orderID))
        
        dataUpdateNotifocation?.onAcceptOrderData.subscribe(on: self) { (isUpdate) in
            if isUpdate {
                
                self.accept.isEnabled = false
                self.accept.backgroundColor = UIColor.gray
                
                self.order.accepted = "True"
            }
            
            
        }
        
    }
    
}


extension AdminDetailsViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if adminProducts == nil {
            return 0
        }
        return adminProducts.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! adminDetailsTableViewCell
        
        let product = adminProducts[indexPath.row]
        
        cell.productName?.text = "\(product.productName)"
        cell.price?.text = "\(product.actualPrice)"
        cell.count?.text = "\(product.Quantity) العدد: "
        cell.category?.text = "\(product.catName)"
        
        
        cell.productImage.sd_setImage(with: URL(string: (product.prodImage)), placeholderImage: UIImage(named: "placeholder.png"))
        
        cell.selectionStyle = .none;
        
        return cell
    }
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
}


