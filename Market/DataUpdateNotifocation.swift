//
//  DataUpdateNotifocation.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/24/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import Signals

class DataUpdateNotifocation {

    let onCategoryData = Signal<(Bool)>()
    let onAdminProductData = Signal<([AdminProduct])>()
    let onOrderData = Signal<([Order])>()
    let onUpdateUserData = Signal<(Bool)>()
    let onRegisterTokenData = Signal<(Bool)>()
    let onAcceptOrderData = Signal<(Bool)>()
    let onCheckoutData = Signal<(Bool)>()
    let onProductData = Signal<(Bool)>()

}
