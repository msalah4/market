//
//  ServiceManager.swift
//  Dealights
//
//  Created by Manar Magdy on 3/22/17.
//  Copyright © 2017 Manar Magdy. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash
import AEXML




class ServiceManager {

    static func retrieveData(actionName: String, children: [String : String]?, elementName: [String], getStringResult:(Bool) = false , completion: @escaping (_ result:  [[String: Any]]?) -> Void) -> Void {
        
        
//        let hud = MBProgressHUD.showAdded(to: (getTopViewController()?.view) ?? UIView(), animated: true)
//        hud.mode = .indeterminate
//        hud.label.text = "Loading"
        
        var dataList: [[String: Any]]?
        
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:soapenv" : "http://schemas.xmlsoap.org/soap/envelope/", "xmlns:tem" : "http://tempuri.org/"]
        let envelope = soapRequest.addChild(name: "soapenv:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soapenv:Body")
        let actionBody = body.addChild(name: actionName)
        
        if let chs = children {
            for (key, value) in chs {
                actionBody.addChild(name: key, value: value, attributes: [:])
            }
        }
        
        let soapLenth = String(soapRequest.xml.characters.count)
        
        let theURL = NSURL(string: "http://smartboxsa.com/Old/Marketservice.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8)
        
        Alamofire.request(mutableR)
            .responseString { response in
                
                if let xmlString = response.result.value {
                    
                    
                    let xml = SWXMLHash.parse(xmlString)
                    var body =  xml["soap:Envelope"]["soap:Body"]
                    
                    for item in elementName {
                        body = body[item]
                    }
                    
                    if let usersElement = body.element {
                        
                        let getResult = usersElement.text
                        
                        if getStringResult {
                            completion([["result":getResult]])
                            return
                        }
                        
                        dataList = ServiceManager.convertToDictionary(text: getResult)
                        
                    }
                    completion(dataList)
                } else {
                    print("error fetching XML")
                    completion(nil)
//                    Helper.showFloatAlert(title: NSLocalizedString("error.network.unreachable", comment: ""), subTitle:"" , type: Constants.AlertType.AlertError)
                    

                }
                
//                DispatchQueue.main.async {
////                    hud.hide(animated: true)
//                }
                
        }
    }

    private class func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    

    
}
