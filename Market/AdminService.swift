//
//  AdminService.swift
//  Market
//
//  Created by Mohamed Salah on 10/9/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AdminService: NSObject {
    
    class func fetchTodayOrders(){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:GetOrders",
                                    children: nil,
                                    elementName: ["GetOrdersResponse", "GetOrdersResult"],
                                    completion: {
                                        result in
                                        
                                        var orders = [Order]()
                                        
                                        if result == nil {
                                            notify.onOrderData.fire(orders)
                                        }
                                        
                                        
                                        for jsonOrder in result! {
                                            let order = Order()
                                            
                                            order.orderID = jsonOrder["OrderID"] as! Int
                                            order.marketCode = jsonOrder["MarketCode"] as! String
                                            order.date = jsonOrder["OrderDate"] as! String
                                            order.total = jsonOrder["TotalPrice"] as! String
                                            order.accepted = jsonOrder["Accepted"] as! String
                                            
                                            orders.append(order)
                                        }
                                        
                                        
                                        
                                        // Notify
                                        notify.onOrderData.fire(orders)
        })
        
        
    }
    
    class func fetchProductsOfOrder(OrderID orderID:String){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:GetProductByID",
                                    children: ["tem:ProductID":orderID],
                                    elementName: ["GetProductByIDResponse", "GetProductByIDResult"],
                                    completion: {
                                        result in
                                        
                                        var products = [AdminProduct]()
                                        
                                        if result == nil {
                                            notify.onAdminProductData.fire(products)
                                        }
                                        
                                        
                                        for jsonOrder in result! {
                                            let adminProduct = AdminProduct()
                                            
                                            adminProduct.itemID = jsonOrder["ItemID"] as! Int
                                            adminProduct.productName = jsonOrder["ProductName"] as! String
                                            adminProduct.mainPrice = jsonOrder["MainPrice"] as! String
                                            adminProduct.mainPriceInSale = jsonOrder["MainPriceInSale"] as! String
                                            adminProduct.Quantity = jsonOrder["Quantity"] as! String
                                            adminProduct.unit = jsonOrder["Unit"] as! String
                                            adminProduct.actualPrice = jsonOrder["ActualPrice"] as! String
                                            adminProduct.prodImage = jsonOrder["ProdImg"] as! String
                                            adminProduct.catName = jsonOrder["CatName"] as! String
                                            
                                            products.append(adminProduct)
                                        }
                                        
                                        
                                        
                                        // Notify
                                        notify.onAdminProductData.fire(products)
        })
        
        
    }
    
    
    class func acceptOrder(OrderID orderID:String){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:AcceptOrder",
                                    children: ["tem:ProductID":orderID],
                                    elementName: ["AcceptOrderResponse", "AcceptOrderResult"],
                                    getStringResult: true,
                                    completion: {
                                        result in
                                        
                                        
                                        if result == nil {
                                            notify.onAcceptOrderData.fire(false)
                                        }
                                        
                                        let value = result![0]["result"] as! String
                                        
                                        if value.characters.count > 10 {
                                            notify.onAcceptOrderData.fire(false)
                                        }
                                        
                                        notify.onAcceptOrderData.fire(true)
                            })
        
    }
    

}
