//
//  ViewController.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit


class ParentViewController: UIViewController {
    
    var barButton:BadgedBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let image = UIImage(imageLiteralResourceName: "shopping-cart")
        let buttonFrame: CGRect = CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height)
        barButton = BadgedBarButtonItem(
            startingBadgeValue: 0,
            frame: buttonFrame,
            image: image
        )
        
        barButton?.addTarget(self, action: #selector(cartBarButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem =  barButton
        
        //        let leftBarButton = barButton
        
        //        navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let cartPresenter = CardPresenter.getSharedInstance()
        cartPresenter.barButton = barButton
        cartPresenter.updateCartBadge()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cartBarButtonTapped(_ sender:Any) {
        
        let cartView = self.storyboard?.instantiateViewController(withIdentifier: "cart")
        self.show(cartView!, sender: nil)
        
    }
    
}

