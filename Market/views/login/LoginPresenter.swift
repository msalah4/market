//
//  LoginPresenter.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import RealmSwift

class LoginPresenter: NSObject {

    
    func login(_ listOfinputs:[String]){
        
        let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
        let user = User()
        
        let userData = ["tem:MarketCode":listOfinputs[0], "tem:Password":listOfinputs[1]]
        
        // 1- Request the new data for Categories
        ServiceManager.retrieveData(actionName: "tem:UserLogin",
                                    children: userData,
                                    elementName: ["UserLoginResponse", "UserLoginResult"],
                                    getStringResult:true,
                                    completion: {
                                        result in
                                        
                                        if result == nil {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        let value = result![0]["result"] as! String
                                        
                                        if value.characters.count > 10 {
                                            notify.onUpdateUserData.fire(false)
                                        }
                                        
                                        // Parse data
                                        let realm = try! Realm()
                                        
                                        user.password = listOfinputs[1]
                                        user.idCode = listOfinputs[0]
                                        user.loginCode = value
                                        
                                        try! realm.write {
                                            realm.add(user, update: true)
                                        }
                                        
                                        // Notify
                                        notify.onUpdateUserData.fire(true)
        })
        
        
    }
    
    
    
}
