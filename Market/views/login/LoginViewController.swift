//
//  LoginViewController.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift
class LoginViewController: UIViewController {
    
    
    static let adminCode = "1000000"
    static let adminPass = "0000001"
    
    @IBOutlet weak var test: UITextView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    var hub:YBHud?
    let loginPresenter:LoginPresenter = LoginPresenter()
    let notify:DataUpdateNotifocation = (UIApplication.shared.delegate as! AppDelegate).notify
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notify.onUpdateUserData.subscribe(on: self) { (isUpdated) in
            self.hub?.dismiss(animated: true)
            if isUpdated {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "home")
                self.navigationController?.viewControllers = [viewController!]
            } else {
                let alert = UIAlertController(title: "خطاء", message: " حدث خطاء ما ،يرجي المحاولة لاحقا", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func loginDidClicked(_ sender: UIButton) {
        
        if (userName.text?.isEmpty)! {
            let alert = UIAlertController(title: "خطاء", message: "برجاء ملئ الخانات المطلوبة", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if LoginViewController.adminCode == userName.text && LoginViewController.adminPass == password.text {
            let adminViewController = self.storyboard?.instantiateViewController(withIdentifier: "admin")
            
            let realm = try! Realm()
            let user = User()
            
            user.idCode = LoginViewController.adminCode
            user.password = LoginViewController.adminPass
            
            try! realm.write {
                realm.delete(realm.objects(User.self))
                realm.add(user, update: true)
            }
            
            
            self.navigationController?.setViewControllers([adminViewController!], animated: true)
        }
        
        hub = YBHud(hudType: .triplePulse)
        hub?.dimAmount = 0.7
        hub?.show(in: self.view, animated: true)
        
        
        loginPresenter.login([userName.text!, password.text!])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
