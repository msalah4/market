//
//  ProductsTableViewCell.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/14/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var addToCart: UIButton!
    
    var productID:String?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func addToCart(_ sender: Any) {
        
        let cartPresenter = CardPresenter.getSharedInstance()
        cartPresenter.addProduct(productID: productID!)
        addToCart.isEnabled = false
        addToCart.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
