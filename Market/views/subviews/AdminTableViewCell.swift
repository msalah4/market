//
//  AdminTableViewCell.swift
//  Market
//
//  Created by Mohammed Salah on 10/7/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AdminTableViewCell: UITableViewCell {
    
    @IBOutlet weak var orderCode: UILabel!
    @IBOutlet weak var marketCode: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var accepted: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
