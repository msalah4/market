//
//  CellaManager.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/19/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class CellaManager {

   static var tableView: UITableView?
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func registerTableView(_ table: UITableView) {
        self.tableView = table
        
        self.tableView?.register(FieldCell.self, forCellReuseIdentifier: CellsTypes.normal.rawValue)
    }
    
    class func instanceFromNib(_ type:CellsTypes) -> RegTableViewCell {
                
//        if type == .normal {
           return UINib(nibName: "FieldCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FieldCell
//        } else {
//            return UINib(nibName: "RegTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RegTableViewCell
//        }
        
        
    }
 
    
}
