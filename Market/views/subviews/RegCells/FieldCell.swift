//
//  FieldCell.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/19/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

enum FieldType:String {
    case Optional = "اختياري";
    case Mandatory = "اجباري";
}

class FieldCell: RegTableViewCell {

    @IBOutlet weak var txtfield: UITextField!
    @IBOutlet weak var title: UILabel!
    
    var fieldType: FieldType = .Optional
    
    static var nibFileName = "RegTableViewCell"
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func cellDidLoad(_ type:CellsTypes) {
        // Drawing code
        super.cellDidLoad(type)
        
        if type == .Email {
            self.txtfield.keyboardType = .emailAddress
        } else if type == .Password {
            self.txtfield.isSecureTextEntry = true
        } else if type == .Phone {
            self.txtfield.keyboardType = .phonePad
        }
        
        
        
        
        self.txtfield.delegate = self
    }
    
    class func instanceFromNib() -> FieldCell {
        return UINib(nibName: "FieldCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FieldCell
    }
    
    override func inputValue () -> String{
        return self.txtfield.text!
    }
    
    override class func getNibFileName() -> String{
        return "FieldCell"
    }

}

extension FieldCell: UITextFieldDelegate{
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
