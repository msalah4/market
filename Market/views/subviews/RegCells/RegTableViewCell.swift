//
//  RegTableViewCell.swift
//  Market
//
//  Created by Mohammed Salah on 9/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

enum CellsTypes : String {
    case normal = "normal";
    case Password = "password";
    case Phone = "phone";
    case Email = "email";
}

class RegTableViewCell: UITableViewCell {
    
    var cellType:CellsTypes?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellDidLoad(_ type:CellsTypes) {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func inputValue() -> String {
        return ""
    }
    
    class func getNibFileName() -> String{
        return "RegTableViewCell"
    }
}
