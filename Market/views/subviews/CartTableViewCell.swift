//
//  CartTableViewCell.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/14/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit



class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceBeforeDiscount: UILabel!
    @IBOutlet weak var discount: UILabel!
    var productID:String?
    var cartPresenter:CardPresenter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func pluseProduct(_ sender: UIButton) {
        let newCount:Int = (cartPresenter?.plusCounOfProduct(productID: self.productID!))!
        self.count.text = String(describing: newCount)
        
    }
    
    @IBAction func minusProduct(_ sender: UIButton) {
        let newCount:Int = (cartPresenter?.minusCounOfProduct(productID: self.productID!))!
        self.count.text = String(describing: newCount)
    }
    
}
