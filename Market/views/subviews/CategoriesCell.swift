//
//  CategoriesCell.swift
//  Market
//
//  Created by Orange Labs Cairo on 9/28/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell {

    
    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var catTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
